#include <malloc.h> // for malloc() and free()
#include <stdio.h> // for input & output
#include <math.h> // for fabs and fmax
#include <time.h> // for time measurement

int write_array(const float * const array, const size_t size, const char* const filename) {
    FILE* fout = fopen(filename, "wb");
    if (fout == NULL) {
        return -1;
    }
    if (fwrite(array, sizeof(float), size, fout) != size) {
        fclose(fout);
        return -1;
    }
    if (fclose(fout) < 0) {
        return -1;
    }
    return 0;
}

static float calc_phi(const float * const phi, const float * const space, const float CONST_5, const float CONST_6,
                      const float CONST_7, const float CONST_8, const float CONST_9,
                      const int index_ij, const int index_idecj, const int index_iincj) {
    return CONST_5 * (phi[index_ij - 1] + phi[index_ij + 1]) +
           CONST_6 * (phi[index_idecj] + phi[index_iincj]) +
           CONST_7 * (phi[index_idecj - 1] + phi[index_idecj + 1] + phi[index_iincj - 1] + phi[index_iincj + 1]) +
           CONST_8 * space[index_ij] +
           CONST_9 * (space[index_idecj] + space[index_iincj] + space[index_ij - 1] + space[index_ij + 1]);
}

static float calc_r(const float xj, const float xs1, const float xs2, const float yi, const float ys1, const float ys2,
                    const float r_sqr) {
    if ((xj - xs1) * (xj - xs1) + (yi - ys1) * (yi - ys1) < r_sqr) {
        return 0.1;
    } else if ((xj - xs2) * (xj - xs2) + (yi - ys2) * (yi - ys2) < r_sqr) {
        return -0.1;
    }
    return 0.0;
}

static float *get_space(const int x, const int y, const float XA, const float  YA, const float HX, const float HY,
                        const float XS1, const float XS2, const float YS1, const float YS2, const float RSQR) {
    float* res = malloc(x * y * sizeof(float));
    if (res == NULL) {
        return NULL;
    }
    float y_i = YA;
    for(int ix = 0; ix < y * x; ix += x) {
        float x_j = XA;
        for (int j = 0; j < x; ++j) {
            res[ix + j] = calc_r(x_j, XS1, XS2, y_i, YS1, YS2, RSQR);
            x_j += HX;
        }
        y_i += HY;
    }
    return res;
}

static int calculations(const int x, const int y, const int steps, float * const phi) {
    if (x * y < 0) {
        return -1;
    }
    const float XA = 0.0;
    const float XB = 4.0;
    const float YA = 0.0;
    const float YB = 4.0;
    const float SUBX = XB - XA;
    const float SUBY = YB - YA;
    const float HX = SUBX / (x - 1);
    const float HY = SUBY / (y - 1);
    const float RSQR = 0.1 * fmin(XB - XA, YB - YA);
    const float XS1 = XA + (XB - XA) / 3;
    const float YS1 = YA + (YB - YA) * 2 / 3;
    const float XS2 = XA + (XB - XA) * 2 / 3;
    const float YS2 = YA + (YB - YA) / 3;
    const float CONST_1 = 1 / (5 * (1 / (HX * HX) + 1 / (HY * HY)));
    const float CONST_2 = 0.5 * (5 / (HX * HX) - 1 / (HY * HY));
    const float CONST_3 = 0.5 * (5 / (HY * HY) - 1 / (HX * HX));
    const float CONST_4 = 0.25 * (1 / (HX * HX) + 1/ (HY * HY));
    const float CONST_5 = CONST_1 * CONST_2;
    const float CONST_6 = CONST_1 * CONST_3;
    const float CONST_7 = CONST_1 * CONST_4;
    const float CONST_8 = CONST_1 * 2;
    const float CONST_9 = CONST_1 * 0.25;
    float* const space = get_space(x, y, XA, YA, HX, HY, XS1, XS2, YS1, YS2, RSQR);
    if (space == NULL) {
        return -1;
    }
    for (int n = 0; n <= steps; ++n) {
//        float delta = -1;
        for (int ix = x; ix < x*(y - 1); ix += x) {
            for (int j = 1; j < x - 1; ++j) {
                const int index_ij = ix + j;
                const int index_idecj = index_ij - x;
                const int index_iincj = index_ij + x;
                phi[index_ij] = calc_phi(phi, space, CONST_5, CONST_6, CONST_7, CONST_8, CONST_9, index_ij, index_idecj, index_iincj);
            }
        }
//        printf("%f\n", delta);
    }
    free(space);
    return 0;
}

int main() {
    FILE* const in  = fopen ("in.txt", "r");
    if (in == NULL) {
        return 1;
    }
    int n_x, n_y, n_steps;
    if (fscanf(in, "%d%d%d", &n_x, &n_y, &n_steps) < 3) {
        fclose(in);
        return 1;
    }
    fclose(in);
    float* const phi = calloc (n_x * n_y, sizeof(float));
    if (phi == NULL) {
        return 1;
    }
    struct timespec mt1, mt2;
    puts("start calculating");
    clock_gettime (CLOCK_REALTIME, &mt1);
    int ret = 0;
    if (calculations(n_x, n_y, n_steps, phi) < 0) {
        ret = 1;
    }
    clock_gettime (CLOCK_REALTIME, &mt2);
    printf("time: %lf", (float) (mt2.tv_sec - mt1.tv_sec) + (float) (mt2.tv_nsec - mt1.tv_nsec) / 1000000000);
    if (write_array(phi, n_x * n_y, "out.dat") < 0) {
        ret = 1;
    }
    free(phi);
    return ret;
}
