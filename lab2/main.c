#include <xmmintrin.h>
#include <stdio.h> // for input & output
#include <math.h> // for fabs and fmax
#include <time.h> // for time measurement

int write_array(const float * const array, const size_t size, const char* const filename) {
    FILE* fout = fopen(filename, "wb");
    if (fout == NULL) {
        return -1;
    }
    if (fwrite(array, sizeof(float), size, fout) != size) {
        fclose(fout);
        return -1;
    }
    if (fclose(fout) < 0) {
        return -1;
    }
    return 0;
}

static float calc_r(const float xj, const float xs1, const float xs2, const float yi, const float ys1, const float ys2,
                    const float r_sqr) {
    if ((xj - xs1) * (xj - xs1) + (yi - ys1) * (yi - ys1) < r_sqr) {
        return 0.1;
    } else if ((xj - xs2) * (xj - xs2) + (yi - ys2) * (yi - ys2) < r_sqr) {
        return -0.1;
    }
    return 0.0;
}

static float* _get_additive(const float* const space, int x, int y, float CONST_8, float CONST_9) {
    float* space2 = malloc(x * y * sizeof(float));
    for (int ix = x; ix < x*(y - 1); ix += x) {
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            const int index_iincj = index_ij + x;
            space2[index_ij] = CONST_8 * space[index_ij] +
                               CONST_9 * (space[index_idecj] + space[index_iincj] + space[index_ij - 1] + space[index_ij + 1]);
        }
    }
    return space2;
}

static float *get_additive(const int x, const int y, const float XA, const float  YA, const float HX, const float HY,
                           const float XS1, const float XS2, const float YS1, const float YS2, const float RSQR,
                           const float CONST_8, const float CONST_9) {
    float* space = malloc(x * y * sizeof(float));
    if (space == NULL) {
        return NULL;
    }
    float y_i = YA;
    for(int ix = 0; ix < y * x; ix += x) {
        float x_j = XA;
        for (int j = 0; j < x; ++j) {
            space[ix + j] = calc_r(x_j, XS1, XS2, y_i, YS1, YS2, RSQR);
            x_j += HX;
        }
        y_i += HY;
    }
    float* res = _get_additive(space, x, y, CONST_8, CONST_9);
    free(space);
    return res;
}

static __m128 one_left_three_right(__m128 left, __m128 right) {
    __m128 lr = _mm_shuffle_ps(left, right, 0b01001110);
    return _mm_shuffle_ps(lr, right, 0b10011001);
}

static __m128 three_left_one_right(__m128 left, __m128 right) {
    __m128 lr = _mm_shuffle_ps(left, right, 0b00001100);
    return _mm_shuffle_ps(left, lr, 0b10011001);
}

static int calculations(const int x, const int y, const int steps, float * phi, float * phi_prev) {
    if (x * y < 0) {
        return -1;
    }
    const float XA = 0.0;
    const float XB = 4.0;
    const float YA = 0.0;
    const float YB = 4.0;
    const float SUBX = XB - XA;
    const float SUBY = YB - YA;
    const float HX = SUBX / (x - 1);
    const float HY = SUBY / (y - 1);
    const float RSQR = 0.1 * fmin(XB - XA, YB - YA);
    const float XS1 = XA + (XB - XA) / 3;
    const float YS1 = YA + (YB - YA) * 2 / 3;
    const float XS2 = XA + (XB - XA) * 2 / 3;
    const float YS2 = YA + (YB - YA) / 3;
    const float CONST_1 = 1 / (5 * (1 / (HX * HX) + 1 / (HY * HY)));
    const float CONST_2 = 0.5 * (5 / (HX * HX) - 1 / (HY * HY));
    const float CONST_3 = 0.5 * (5 / (HY * HY) - 1 / (HX * HX));
    const float CONST_4 = 0.25 * (1 / (HX * HX) + 1/ (HY * HY));
    const float CONST_5 = CONST_1 * CONST_2;
    const float CONST_6 = CONST_1 * CONST_3;
    const float CONST_7 = CONST_1 * CONST_4;
    const float CONST_8 = CONST_1 * 2;
    const float CONST_9 = CONST_1 * 0.25;
    float* const additive = get_additive(x, y, XA, YA, HX, HY, XS1, XS2, YS1, YS2, RSQR, CONST_8, CONST_9);
    if (additive == NULL) {
        return -1;
    }
    __m128 *vphi = (__m128*) phi;
    __m128 *vphi_prev = (__m128*) phi_prev;
    __m128 *vadditive = (__m128*) additive;
    __m128 vCONST5 = _mm_set1_ps(CONST_5);
    __m128 vCONST6 = _mm_set1_ps(CONST_6);
    __m128 vCONST7 = _mm_set1_ps(CONST_7);
/*
    for (int n = 0; n <= steps; ++n) {
        void* tmp = vphi;
        vphi = vphi_prev;
        vphi_prev = tmp;
        for (int ix = x / 4; ix < x / 4 * (y - 1); ix += x / 4) {
            for (int j = 1; j < x / 4 - 1; ++j) {
                const int index_ij = ix + j;
                const int index_idecj = index_ij - x / 4;
                const int index_iincj = index_ij + x / 4;
                __m128 l_up = _mm_loadu_ps((float*)(vphi_prev + index_iincj) - 1);
                __m128 r_up = _mm_loadu_ps((float*)(vphi_prev + index_iincj) + 1);
                __m128 l_mid = _mm_loadu_ps((float*)(vphi_prev + index_ij) - 1);
                __m128 r_mid = _mm_loadu_ps((float*)(vphi_prev + index_ij) + 1);
                __m128 l_down = _mm_loadu_ps((float*)(vphi_prev + index_idecj) - 1);
                __m128 r_down = _mm_loadu_ps((float*)(vphi_prev + index_idecj) + 1);
                vphi[index_ij] = vCONST5 * (l_mid + r_mid) +
                                vCONST6 * (vphi_prev[index_idecj] + vphi_prev[index_iincj]) +
                                vCONST7 * (l_down + r_down + l_up + r_up) +
                                vadditive[index_ij];
            }
        }
    }
*/
/*
    for (int n = 0; n <= steps; ++n) {
        void* tmp = vphi;
        vphi = vphi_prev;
        vphi_prev = tmp;
        for (int ix = x / 4; ix < x / 4 * (y - 1); ix += x / 4) {
            // center zero step
            __m128 c_up = vphi_prev[ix + x / 4];
            __m128 c_mid = vphi_prev[ix];
            __m128 c_down = vphi_prev[ix - x / 4];
            // right zero step
            __m128 r_up = vphi_prev[ix + x / 4 + 1];
            __m128 r_mid = vphi_prev[ix + 1];
            __m128 r_down = vphi_prev[ix - x / 4 + 1];
            for (int j = 1; j < x / 4 - 1; ++j) {
                const int index_ij = ix + j;
                const int index_iincj = index_ij + x / 4;
                const int index_idecj = index_ij - x / 4;
                __m128 l_up = c_up;
                __m128 l_mid = c_mid;
                __m128 l_down = c_down;
                c_up = r_up;
                c_mid = r_mid;
                c_down = r_down;
                r_up = vphi_prev[index_iincj + 1];
                r_mid = vphi_prev[index_ij + 1];
                r_down = vphi_prev[index_idecj + 1];
                __m128 l_up_u = one_left_three_right (l_up, c_up);
                __m128 r_up_u = three_left_one_right (c_up, r_up);
                __m128 l_mid_u = one_left_three_right (l_mid, c_mid);
                __m128 r_mid_u = three_left_one_right (c_mid, r_mid);
                __m128 l_down_u = one_left_three_right (l_down, c_down);
                __m128 r_down_u = three_left_one_right (c_down, r_down);
                vphi[index_ij] = vCONST5 * (l_mid_u + r_mid_u) +
                                 vCONST6 * (vphi_prev[index_idecj] + vphi_prev[index_iincj]) +
                                 vCONST7 * (l_down_u + r_down_u + l_up_u + r_up_u) +
                                 vadditive[index_ij];
            }
        }
    }
*/
    for (int n = 0; n <= steps; ++n) {
        void* tmp = phi;
        phi = phi_prev;
        phi_prev = tmp;
        for (int ix = x; ix < x*(y - 1); ix += x) {
#pragma omp simd
            for (int j = 1; j < x - 1; ++j) {
                const int index_ij = ix + j;
                const int index_idecj = index_ij - x;
                const int index_iincj = index_ij + x;
                phi[index_ij] = CONST_5 * (phi_prev[index_ij - 1] + phi_prev[index_ij + 1]) +
                                CONST_6 * (phi_prev[index_idecj] + phi_prev[index_iincj]) +
                                CONST_7 * (phi_prev[index_idecj - 1] + phi_prev[index_idecj + 1] + phi_prev[index_iincj - 1] + phi_prev[index_iincj + 1]) +
                                additive[index_ij];
            }
        }
    }
    free(additive);
    return 0;
}

int main() {
    FILE* const in  = fopen ("in.txt", "r");
    if (in == NULL) {
        return 1;
    }
    int n_x, n_y, n_steps;
    if (fscanf(in, "%d%d%d", &n_x, &n_y, &n_steps) < 3) {
        fclose(in);
        return 1;
    }
    if (n_x % 4 != 0) {
        perror("x is not a multiple of 4 =(");
        return 1;
    }
    fclose(in);
    float* const phi = calloc (n_x * n_y * 2, sizeof(float));
    if (phi == NULL) {
        return 1;
    }
    float* const phi_prev = calloc(n_x * n_y * 2, sizeof(float));
    if (phi_prev == NULL) {
        free(phi);
        return 1;
    }
    struct timespec mt1, mt2;
    puts("start calculating");
    clock_gettime (CLOCK_REALTIME, &mt1);
    int ret = 0;
    if (calculations(n_x, n_y, n_steps, phi, phi_prev) < 0) {
        ret = 1;
    }
    clock_gettime (CLOCK_REALTIME, &mt2);
    printf("time: %lf\n", (float) (mt2.tv_sec - mt1.tv_sec) + (float) (mt2.tv_nsec - mt1.tv_nsec) / 1000000000);
    if (write_array(phi, n_x * n_y, "out.dat") < 0) {
        ret = 1;
    }
    free(phi);
    free(phi_prev);
    return ret;
}
