#include <stdio.h> // for input & output
#include <math.h> // for fabs and fmax
#include <time.h> // for time measurement
#include <malloc.h> // for time measurement

int write_array(const float * const array, const size_t size, const char* const filename) {
    FILE* fout = fopen(filename, "wb");
    if (fout == NULL) {
        return -1;
    }
    if (fwrite(array, sizeof(float), size, fout) != size) {
        fclose(fout);
        return -1;
    }
    if (fclose(fout) < 0) {
        return -1;
    }
    return 0;
}

void swap_ptr(float** a, float** b) {
    float* tmp = *a;
    *a = *b;
    *b = tmp;
}

static float calc_r(const float xj, const float xs1, const float xs2, const float yi, const float ys1, const float ys2,
                    const float r_sqr) {
    if ((xj - xs1) * (xj - xs1) + (yi - ys1) * (yi - ys1) < r_sqr) {
        return 0.1;
    } else if ((xj - xs2) * (xj - xs2) + (yi - ys2) * (yi - ys2) < r_sqr) {
        return -0.1;
    }
    return 0.0;
}

static float* _get_additive(const float* const space, int x, int y, float CONST_8, float CONST_9) {
    float* space2 = malloc(x * y * sizeof(float));
    for (int ix = x; ix < x*(y - 1); ix += x) {
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            const int index_iincj = index_ij + x;
            space2[index_ij] = CONST_8 * space[index_ij] +
                               CONST_9 * (space[index_idecj] + space[index_iincj] + space[index_ij - 1] + space[index_ij + 1]);
        }
    }
    return space2;
}

static float *get_additive(const int x, const int y, const float XA, const float  YA, const float HX, const float HY,
                           const float XS1, const float XS2, const float YS1, const float YS2, const float RSQR,
                           const float CONST_8, const float CONST_9) {
    float* space = malloc(x * y * sizeof(float));
    if (space == NULL) {
        return NULL;
    }
    float y_i = YA;
    for(int ix = 0; ix < y * x; ix += x) {
        float x_j = XA;
        for (int j = 0; j < x; ++j) {
            space[ix + j] = calc_r(x_j, XS1, XS2, y_i, YS1, YS2, RSQR);
            x_j += HX;
        }
        y_i += HY;
    }
    float* res = _get_additive(space, x, y, CONST_8, CONST_9);
    free(space);
    return res;
}

inline static float calc_phi(float CONST_5, float CONST_6, float CONST_7, float* phi_prev, int index_ij, int x, float* additive) {
    const int index_idecj = index_ij - x;
    const int index_iincj = index_ij + x;
    return CONST_5 * (phi_prev[index_ij - 1] + phi_prev[index_ij + 1]) +
    CONST_6 * (phi_prev[index_idecj] + phi_prev[index_iincj]) +
    CONST_7 * (phi_prev[index_idecj - 1] + phi_prev[index_idecj + 1] + phi_prev[index_iincj - 1] + phi_prev[index_iincj + 1]) +
           additive[index_ij];
}

inline static void calc_8_steps(float CONST_5, float CONST_6, float CONST_7, float* phi_v, float* phi_u, int x, int y, float* additive) {
    for (int ix = x; ix <= x * 7 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 6 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_v[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 5 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 4 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_v[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 3 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 2 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_v[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij, x, additive);
        }
    }
    {
        int ix = x;
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
    }
    for (int ix = x * 8; ix < x*(y - 1); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            phi_v[index_idecj] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_idecj, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec2x = ix + j - 2 * x;
            phi_u[index_ij_dec2x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec2x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec5x = ix + j - 5 * x;
            phi_v[index_ij_dec5x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec5x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec6x = ix + j - 6 * x;
            phi_u[index_ij_dec6x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec6x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec7x = ix + j - 7 * x;
            phi_v[index_ij_dec7x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec7x, x, additive);
        }
    }
    {
        int ix = x*(y - 1);
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            phi_v[index_idecj] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_idecj, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec2x = ix + j - 2 * x;
            phi_u[index_ij_dec2x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec2x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*y;
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec2x = ix + j - 2 * x;
            phi_u[index_ij_dec2x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec2x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*(y + 1);
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*(y + 2);
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*(y - 1);
#pragma omp simd
        for (int k = 5; k >= 1; --k) {
            for (int j = 1; j < x - 1; ++j) {
                const int index_ij_deckx = ix + j - k * x;
                phi_v[index_ij_deckx] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_deckx, x, additive);
            }
        }
    }
    {
        int ix = x*(y - 1);
#pragma omp simd
        for (int k = 6; k >= 1; --k) {
            for (int j = 1; j < x - 1; ++j) {
                const int index_ij_deckx = ix + j - k * x;
                phi_u[index_ij_deckx] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_deckx, x, additive);
            }
        }
    }
    {
        int ix = x*(y - 1);
#pragma omp simd
        for (int k = 7; k >= 1; --k) {
            for (int j = 1; j < x - 1; ++j) {
                const int index_ij_deckx = ix + j - k * x;
                phi_v[index_ij_deckx] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_deckx, x, additive);
            }
        }
    }
}

inline static void calc_last_5_steps(float CONST_5, float CONST_6, float CONST_7, float* phi_v, float* phi_u, int x, int y, float* additive) {
    for (int ix = x; ix <= x * 4 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 3 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_v[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij, x, additive);
        }
    }
    for (int ix = x; ix <= x * 2 && ix < x*(y - 2); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
    }
    {
        int ix = x;
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_v[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij, x, additive);
        }
    }
    for (int ix = x * 5; ix < x*(y - 1); ix += x) {
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            phi_u[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            phi_v[index_idecj] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_idecj, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec2x = ix + j - 2 * x;
            phi_u[index_ij_dec2x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec2x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*(y - 1);
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            phi_v[index_idecj] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_idecj, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec2x = ix + j - 2 * x;
            phi_u[index_ij_dec2x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec2x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*y;
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec2x = ix + j - 2 * x;
            phi_u[index_ij_dec2x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec2x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*(y + 1);
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec3x = ix + j - 3 * x;
            phi_v[index_ij_dec3x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_u, index_ij_dec3x, x, additive);
        }
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
    {
        int ix = x*(y + 2);
#pragma omp simd
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij_dec4x = ix + j - 4 * x;
            phi_u[index_ij_dec4x] = calc_phi(CONST_5, CONST_6, CONST_7, phi_v, index_ij_dec4x, x, additive);
        }
    }
}

static int calculations(const int x, const int y, const int steps, float * phi_u, float * phi_v) {
    if (x * y < 0) {
        return -1;
    }
    const float XA = 0.0;
    const float XB = 4.0;
    const float YA = 0.0;
    const float YB = 4.0;
    const float SUBX = XB - XA;
    const float SUBY = YB - YA;
    const float HX = SUBX / (x - 1);
    const float HY = SUBY / (y - 1);
    const float RSQR = 0.1 * fmin(XB - XA, YB - YA);
    const float XS1 = XA + (XB - XA) / 3;
    const float YS1 = YA + (YB - YA) * 2 / 3;
    const float XS2 = XA + (XB - XA) * 2 / 3;
    const float YS2 = YA + (YB - YA) / 3;
    const float CONST_1 = 1 / (5 * (1 / (HX * HX) + 1 / (HY * HY)));
    const float CONST_2 = 0.5 * (5 / (HX * HX) - 1 / (HY * HY));
    const float CONST_3 = 0.5 * (5 / (HY * HY) - 1 / (HX * HX));
    const float CONST_4 = 0.25 * (1 / (HX * HX) + 1/ (HY * HY));
    const float CONST_5 = CONST_1 * CONST_2;
    const float CONST_6 = CONST_1 * CONST_3;
    const float CONST_7 = CONST_1 * CONST_4;
    const float CONST_8 = CONST_1 * 2;
    const float CONST_9 = CONST_1 * 0.25;
    float* const additive = get_additive(x, y, XA, YA, HX, HY, XS1, XS2, YS1, YS2, RSQR, CONST_8, CONST_9);
    if (additive == NULL) {
        return -1;
    }
    for (int n = 0; n <= steps; n += 8) {
        calc_8_steps(CONST_5, CONST_6, CONST_7, phi_v, phi_u, x, y, additive);
    }
    calc_last_5_steps(CONST_5, CONST_6, CONST_7, phi_v, phi_u, x, y, additive);
    free(additive);
    return 0;
}

int main() {
    FILE* const in  = fopen ("in.txt", "r");
    if (in == NULL) {
        return 1;
    }
    int n_x, n_y, n_steps;
    if (fscanf(in, "%d%d%d", &n_x, &n_y, &n_steps) < 3) {
        fclose(in);
        return 1;
    }
    if (n_x % 4 != 0) {
        perror("x is not a multiple of 4 =(");
        return 1;
    }
    fclose(in);
    float* const phi = calloc (n_x * n_y * 2, sizeof(float));
    if (phi == NULL) {
        return 1;
    }
    float* const phi_prev = calloc(n_x * n_y * 2, sizeof(float));
    if (phi_prev == NULL) {
        free(phi);
        return 1;
    }
    struct timespec mt1, mt2;
    puts("start calculating");
    clock_gettime (CLOCK_REALTIME, &mt1);
    int ret = 0;
    if (calculations(n_x, n_y, n_steps, phi, phi_prev) < 0) {
        ret = 1;
    }
    clock_gettime (CLOCK_REALTIME, &mt2);
    printf("time: %lf\n", (float) (mt2.tv_sec - mt1.tv_sec) + (float) (mt2.tv_nsec - mt1.tv_nsec) / 1000000000);
    if (write_array(phi, n_x * n_y, "out.dat") < 0) {
        ret = 1;
    }
    free(phi);
    free(phi_prev);
    return ret;
}
