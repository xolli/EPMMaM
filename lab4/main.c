#include <stdio.h> // for input & output
#include <math.h> // for fabs and fmax
#include <time.h> // for time measurement
#include <malloc.h> // for malloc and free
#include <omp.h> // OpenMP

inline static float calc_phi(float CONST_5, float CONST_6, float CONST_7, float* phi_prev, int index_ij, int x, float* additive) {
    const int index_idecj = index_ij - x;
    const int index_iincj = index_ij + x;
    return CONST_5 * (phi_prev[index_ij - 1] + phi_prev[index_ij + 1]) +
           CONST_6 * (phi_prev[index_idecj] + phi_prev[index_iincj]) +
           CONST_7 * (phi_prev[index_idecj - 1] + phi_prev[index_idecj + 1] + phi_prev[index_iincj - 1] + phi_prev[index_iincj + 1]) +
           additive[index_ij];
}

int write_array(const float * const array, const size_t size, const char* const filename) {
    FILE* fout = fopen(filename, "wb");
    if (fout == NULL) {
        return -1;
    }
    if (fwrite(array, sizeof(float), size, fout) != size) {
        fclose(fout);
        return -1;
    }
    if (fclose(fout) < 0) {
        return -1;
    }
    return 0;
}

static float calc_r(const float xj, const float xs1, const float xs2, const float yi, const float ys1, const float ys2,
                    const float r_sqr) {
    if ((xj - xs1) * (xj - xs1) + (yi - ys1) * (yi - ys1) < r_sqr) {
        return 0.1;
    } else if ((xj - xs2) * (xj - xs2) + (yi - ys2) * (yi - ys2) < r_sqr) {
        return -0.1;
    }
    return 0.0;
}

static float* _get_additive(const float* const space, int x, int y, float CONST_8, float CONST_9) {
    float* space2 = malloc(x * y * sizeof(float));
#pragma omp parallel for
    for (int ix = x; ix < x*(y - 1); ix += x) {
        for (int j = 1; j < x - 1; ++j) {
            const int index_ij = ix + j;
            const int index_idecj = index_ij - x;
            const int index_iincj = index_ij + x;
            space2[index_ij] = CONST_8 * space[index_ij] +
                               CONST_9 * (space[index_idecj] + space[index_iincj] + space[index_ij - 1] + space[index_ij + 1]);
        }
    }
    return space2;
}

static float *get_additive(const int x, const int y, const float XA, const float  YA, const float HX, const float HY,
                           const float XS1, const float XS2, const float YS1, const float YS2, const float RSQR,
                           const float CONST_8, const float CONST_9) {
    float* space = malloc(x * y * sizeof(float));
    if (space == NULL) {
        return NULL;
    }
    float y_i = YA;
#pragma omp parallel for
    for(int ix = 0; ix < y * x; ix += x) {
        float x_j = XA;
        for (int j = 0; j < x; ++j) {
            space[ix + j] = calc_r(x_j, XS1, XS2, y_i, YS1, YS2, RSQR);
            x_j += HX;
        }
        y_i += HY;
    }
    float* res = _get_additive(space, x, y, CONST_8, CONST_9);
    free(space);
    return res;
}

static int calculations(const int x, const int y, const int steps, float * phi, float * phi_prev) {
    if (x * y < 0) {
        return -1;
    }
    const float XA = 0.0;
    const float XB = 4.0;
    const float YA = 0.0;
    const float YB = 4.0;
    const float SUBX = XB - XA;
    const float SUBY = YB - YA;
    const float HX = SUBX / (x - 1);
    const float HY = SUBY / (y - 1);
    const float RSQR = 0.1 * fmin(XB - XA, YB - YA);
    const float XS1 = XA + (XB - XA) / 3;
    const float YS1 = YA + (YB - YA) * 2 / 3;
    const float XS2 = XA + (XB - XA) * 2 / 3;
    const float YS2 = YA + (YB - YA) / 3;
    const float CONST_1 = 1 / (5 * (1 / (HX * HX) + 1 / (HY * HY)));
    const float CONST_2 = 0.5 * (5 / (HX * HX) - 1 / (HY * HY));
    const float CONST_3 = 0.5 * (5 / (HY * HY) - 1 / (HX * HX));
    const float CONST_4 = 0.25 * (1 / (HX * HX) + 1/ (HY * HY));
    const float CONST_5 = CONST_1 * CONST_2;
    const float CONST_6 = CONST_1 * CONST_3;
    const float CONST_7 = CONST_1 * CONST_4;
    const float CONST_8 = CONST_1 * 2;
    const float CONST_9 = CONST_1 * 0.25;
    float* const additive = get_additive(x, y, XA, YA, HX, HY, XS1, XS2, YS1, YS2, RSQR, CONST_8, CONST_9);
    if (additive == NULL) {
        return -1;
    }

    for (int n = 0; n <= steps; ++n) {
        void* tmp = phi;
        phi = phi_prev;
        phi_prev = tmp;
#pragma omp parallel for
        for (int j = 1; j < x - 1; ++j) {
#pragma omp simd
            for (int ix = x; ix < x*(y - 1); ix += x) {
                const int index_ij = ix + j;
                phi[index_ij] = calc_phi(CONST_5, CONST_6, CONST_7, phi_prev, index_ij, x, additive);
            }
        }
    }
    free(additive);
    return 0;
}

int main() {
    omp_set_num_threads(4);
    FILE* const in  = fopen ("in.txt", "r");
    if (in == NULL) {
        perror("Cannot find in.txt =(");
        return 1;
    }
    int n_x, n_y, n_steps;
    if (fscanf(in, "%d%d%d", &n_x, &n_y, &n_steps) < 3) {
        perror("input error =(");
        fclose(in);
        return 1;
    }
    if (n_x % 4 != 0) {
        perror("x is not a multiple of 4 =(");
        return 1;
    }
    fclose(in);
    float* const phi = calloc (n_x * n_y * 2, sizeof(float));
    if (phi == NULL) {
        return 1;
    }
    float* const phi_prev = calloc(n_x * n_y * 2, sizeof(float));
    if (phi_prev == NULL) {
        free(phi);
        return 1;
    }
    struct timespec mt1, mt2;
    puts("start calculating");
    clock_gettime (CLOCK_REALTIME, &mt1);
    int ret = 0;
    if (calculations(n_x, n_y, n_steps, phi, phi_prev) < 0) {
        ret = 1;
    }
    clock_gettime (CLOCK_REALTIME, &mt2);
    printf("time: %lf\n", (float) (mt2.tv_sec - mt1.tv_sec) + (float) (mt2.tv_nsec - mt1.tv_nsec) / 1000000000);
    if (write_array(phi, n_x * n_y, "out.dat") < 0) {
        ret = 1;
    }
    free(phi);
    free(phi_prev);
    return ret;
}
